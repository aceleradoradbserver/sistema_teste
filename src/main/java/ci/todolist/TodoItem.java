package ci.todolist;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class TodoItem {

    @Id
    @TableGenerator(name = "todo_item_id", table = "sequences", pkColumnName = "sequence_name", valueColumnName = "sequence_number", pkColumnValue = "sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "todo_item_id")
    private Long id;

    private String description;

    public TodoItem() {
    }

    public TodoItem(String name) {
        this(null, name);
    }

    public TodoItem(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

}
